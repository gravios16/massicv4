<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserBandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_bands', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start_date');
            $table->date('end_date');
            $table->smallInteger('present');
            $table->smallInteger('leader');

            $table->integer('musical_role_id')->unsigned();
            $table->foreign('musical_role_id')->references('id')->on('musical_roles');
            $table->integer('band_id')->unsigned();
            $table->foreign('band_id')->references('id')->on('bands');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_bands');
    }
}
