<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandMusicalGenreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('band_musical_genres', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('musical_genre_id')->unsigned();
            $table->foreign('musical_genre_id')->references('id')->on('musical_genres');
            $table->integer('band_id')->unsigned();
            $table->foreign('band_id')->references('id')->on('bands');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('band_musical_genres');
    }
}
