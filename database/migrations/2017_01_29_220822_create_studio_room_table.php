<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudioRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studio_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->double('size')->nullable();
            $table->smallInteger('deleted');

            $table->integer('headquarter_id')->unsigned();
            $table->foreign('headquarter_id')->references('id')->on('headquarters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('studio_rooms');
    }
}
