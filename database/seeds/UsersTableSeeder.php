<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Massimo",
            'last_name' => "Ludeña",
            'mothers_last_name' => "Lezama",
            'phone' => "959941932",
            'dni' => "75572582",
            'email' => 'massimo.ludena.test@gmail.com',
            'password' => bcrypt('secret'),
        ]);
    }
}
