<?php

use Illuminate\Database\Seeder;

class HeadquartersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('headquarters')->insert(['name' => 'Sede 1', 'Direccion sede 1', '7981484', 0, 1]);
    }
}
