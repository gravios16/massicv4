<?php

use Illuminate\Database\Seeder;

class MusicalRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('musical_roles')->insert(['deleted' =>0, 'name' => "Vocal"]);
        DB::table('musical_roles')->insert(['deleted' =>0, 'name' => "Guitarra lider"]);
        DB::table('musical_roles')->insert(['deleted' =>0, 'name' => "Guitarra ritmica"]);
        DB::table('musical_roles')->insert(['deleted' =>0, 'name' => "Bajo"]);
        DB::table('musical_roles')->insert(['deleted' =>0, 'name' => "Percusión"]);
    }
}
