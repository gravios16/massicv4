<?php

use Illuminate\Database\Seeder;

class StudiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('studios')->insert(['name' => 'Massic Studio', 'email' => 'massic@gmail.com', 'deleted' => 0]);
    }
}
