<?php

use Illuminate\Database\Seeder;

class ResourceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('resource_types')->insert(['deleted' =>0, 'name' => "Equipo"]);
        DB::table('resource_types')->insert(['deleted' =>0, 'name' => "Instrumento"]);
    }
}
