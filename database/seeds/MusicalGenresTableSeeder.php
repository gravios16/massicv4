<?php

use Illuminate\Database\Seeder;

class MusicalGenresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "ballenato"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "balada"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "ballenato"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "pop"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "rock"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "salsa"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "tropical"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "popular"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "ranchera"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "techno"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "regge"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "reggeton"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "musica clasica"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "latina"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "electonica"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "jazz"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "infantil"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "rap"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "tango"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "ska"]);
        DB::table('musical_genres')->insert(['deleted' =>0, 'name' => "flamenco"]);
    }
}
