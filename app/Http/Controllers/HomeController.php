<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\Studio\StudioRepositoryInterface;

class HomeController extends Controller
{
    protected $studioRepo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(StudioRepositoryInterface $studioRepo)
    {
        //$this->middleware('auth');
        $this->studioRepo = $studioRepo;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $studios = $this->studioRepo->getAllDetailedByStatus(true);
        //dd($studios);
        return view('home.home', compact('studios'));
    }
}
