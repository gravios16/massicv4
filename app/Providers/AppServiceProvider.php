<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            \App\Repositories\Studio\StudioRepositoryInterface::class,
            \App\Repositories\Studio\StudioEloquentRepository::class
        );

        $this->app->singleton(
            \App\Repositories\Studio\HeadQuarterRepositoryInterface::class,
            \App\Repositories\Studio\HeadQuarterEloquentRepository::class
        );
    }
}
