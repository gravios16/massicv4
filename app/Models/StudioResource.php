<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudioResource extends Model
{
    public function resourceType()
    {
        return $this->BelongsTo('App\Models\ResourceType');
    }
}
