<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudioRoom extends Model
{
    public function resources()
    {
        return $this->hasMany('App\Models\StudioResource');
    }
}
