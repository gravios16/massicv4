<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Headquarter extends Model
{
    protected $fillable = ['name', 'direction', 'phone', 'deleted', 'studio_id'];

    public function studioRooms()
    {
        return $this->hasMany('App\Models\StudioRoom');
    }
}
