<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Studio extends Model
{
    protected $fillable = ['name', 'email', 'deleted'];

    public function headquarters()
    {
        return $this->hasMany('App\Models\HeadQuarter');
    }
}
