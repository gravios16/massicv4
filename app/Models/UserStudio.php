<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserStudio extends Model
{
    protected $fillable = ['user_id', 'studio_id', 'user_studio_type_id'];


}
