<?php
namespace App\Repositories\Headquarter;

interface HeadquarterRepositoryInterface
{
    /**
     * Get all studios
     * @return mixed
     */
    public function getAllActive();

    /**
     * Get post only studio
     * @return mixed
     */
    public function findOnlyActive($id);
}