<?php
namespace App\Repositories\Headquarter;

use App\Repositories\EloquentRepository;

use App\Models\Headquarter;

class HeadquarterEloquentRepository extends EloquentRepository implements HeadquarterRepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Headquarter::class;
    }

    /**
     * Get all studios
     * @return mixed
     */
    public function getAllActive()
    {
        $result = $this->_model->where('deleted', 0)->get();

        return $result->toArray();
    }

    /**
     * Get post only studio
     * @param $id int Post ID
     * @return mixed
     */
    public function findOnlyActive($id)
    {
        $result = $this
            ->_model
            ->where('id', $id)
            ->where('deleted', 0)
            ->first();

        return $result->toArray();
    }
}