<?php
namespace App\Repositories\Studio;

interface StudioRepositoryInterface
{
    /**
     * Get all studios
     * @return mixed
     */
    public function getAllByStatus($status);

    /**
     * Get post only studio
     * @return mixed
     */
    public function findByStatus($id, $status);

    public function getAllDetailedByStatus($status);

    public function getDetail($id);
}