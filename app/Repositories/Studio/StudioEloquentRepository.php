<?php
namespace App\Repositories\Studio;

use App\Repositories\EloquentRepository;

use App\Models\Studio;

class StudioEloquentRepository extends EloquentRepository implements StudioRepositoryInterface
{

    /**
     * get model
     * @return string
     */
    public function getModel()
    {
        return Studio::class;
    }

    /**
     * Get all studios
     * @return mixed
     */
    public function getAllByStatus($status)
    {
        $result = $this->_model->where('deleted', !$status)->get();

        return $result->toArray();
    }

    /**
     * Get post only studio
     * @param $id int Studio ID
     * @return mixed
     */
    public function findByStatus($id, $status)
    {
        $result = $this
            ->_model
            ->where('id', $id)
            ->where('deleted', !$status)
            ->first();

        return $result->toArray();
    }

    public function getAllDetailedByStatus($status)
    {
        $result = $this->_model::with('headquarters.studioRooms.resources.resourceType')->where('deleted', 0)->get();
        return $result->toArray();
    }

    public function getDetail($id){
        $result = $this->_model::with('headquarters.studioRooms.resources.resourceType')->where('id', $id)->first();
        return $result->toArray();
    }
}