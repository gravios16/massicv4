<!DOCTYPE html>
<html lang="en" class="app">
@include('components.header')

<body>
    <section class="vbox">
        @include('components.navbar')
        <section>
            <section class="hbox stretch">
                @include('components.leftnavbar')
                <section id="content">
                    <section class="hbox stretch">
                        <section>
                            <section class="vbox">
                                @yield('content')
                                @if( isset($c_player) && $c_player == true )
                                    @include('components.player')
                                @endif
                            </section>
                        </section>
                        @include('components.rightchat')
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
                </section>
            </section>
        </section>
    </div>
    @include('components.scripts')
</body>
<script type="text/javascript">
	//var socket = io.connect('http://massic.api:4000');
</script>
</html>
