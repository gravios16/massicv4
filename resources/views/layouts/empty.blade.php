<!DOCTYPE html>
<html lang="en" class="app">
@include('components.header')

<body>
    <section class="vbox">
        @include('components.navbar')
        <section>
            @yield('content')
        </section>
    </section>
    @include('components.scripts')
</body>
</html>
