@extends('layouts.app', ['c_player' => true])

@section('content')
<div class="row row-sm hide">
    <div class="col-lg-12">
        <!-- .breadcrumb -->
        <ul class="breadcrumb bg-empty" style="border: none;">
            <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
            <li><a href="#"><i class="fa fa-list-ul"></i> Elements</a></li>
            <li class="active">Components</li>
        </ul>
        <!-- / .breadcrumb -->
    </div>
</div>

<section class="scrollable  padder-lg w-f-md">
    <a href="#" class="pull-right text-muted m-t-lg" data-toggle="class:fa-spin" >
        <i class="icon-refresh i-lg    inline" id="refresh"></i>
    </a><br>
    <h2 class="font-thin m-b">
        {{ $studio["name"] }}
        <span class="musicbar animate inline m-l-sm" style="width:20px;height:20px">
            <span class="bar1 a1 bg-primary lter"></span>
            <span class="bar2 a2 bg-info lt"></span>
            <span class="bar3 a3 bg-success"></span>
            <span class="bar4 a4 bg-warning dk"></span>
            <span class="bar5 a5 bg-danger dker"></span>
        </span>
    </h2>
    <div class="row row-sm">
        <div class="col-md-12">
            <div class="post-item">
                <div class="caption wrapper-lg">
                    <p>
                        Phasellus at ultricies neque, quis malesuada augue. Donec eleifend condimentum nisl eu consectetur. Integer eleifend, nisl venenatis consequat iaculis, lectus arcu malesuada sem, dapibus porta quam lacus eu neque.
                    </p>
                </div>
            </div>
        </div>
    </div>
    @foreach ( $studio["headquarters"] as $headquarter )
        @if( sizeof($headquarter["studio_rooms"]) > 0 )
            <h3 class="font-thin m-b">
                {{ $headquarter["name"] }}
            </h3>
            @foreach ($headquarter["studio_rooms"] as $index => $studio_room)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <a href="{{route('studioProfile', ['id' => $studio['id']])}}" class="">
                                {{$studio_room["name"]}}
                            </a>
                        </h3>
                    </div>
                    <div class="panel-body bg-black">
                        <div class="col-md-6 bg-black" style="padding: 0">

                            <!-- .crousel fade -->
                            <section class="bg-black">
                                <div class="carousel slide " id="c-slide-{{$studio_room['id']}}" style="padding: 30px;">
                                        <ol class="carousel-indicators out">
                                                <li data-target="#c-slide-{{$studio_room['id']}}" data-slide-to="0" class="active"></li>
                                                <li data-target="#c-slide-{{$studio_room['id']}}" data-slide-to="0" class=""></li>
                                        </ol>
                                        <div class="carousel-inner">
                                                <div class="item active">
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="pos-rlt">
                                                            <a href="track-detail1.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="pos-rlt">
                                                            <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="pos-rlt">
                                                            <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                        <div class="pos-rlt">
                                                            <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>

                                        <a class="left carousel-control" href="#c-slide-{{$studio_room['id']}}" data-slide="prev">
                                            <i class="fa fa-angle-left"></i>
                                        </a>
                                        <a class="right carousel-control" href="#c-slide-{{$studio_room['id']}}" data-slide="next">
                                            <i class="fa fa-angle-right"></i>
                                        </a>
                                </div>
                            </section>
                            <!-- / .carousel fade -->
                        </div>
                        <div class="col-md-6 bg-black panel-body" style="padding: 0">
                            <h4>Intrumentos y equipos:</h4>
                            <div class="col-md-12">
                                <small class="text-muted">
                                    <ul>
                                    @foreach ($studio_room["resources"] as $resource)
                                        <li>
                                            {{$resource['name']}} ({{$resource['brand']}})
                                        </li>
                                    @endforeach
                                    </ul>
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <br>
            @endforeach
        @endif
    @endforeach

    <h3 class="font-thin m-b">
        Galería
    </h3>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
      <div class="item">
        <div class="pos-rlt">
          <div class="item-overlay opacity r r-2x bg-black">
            <div class="center text-center m-t-n">
              <a href="#"><i class="fa fa-play-circle i-2x"></i></a>
            </div>
          </div>
          <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
        </div>
        <div class="padder-v">
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis">Tempered Song</a>
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis text-xs text-muted">Miaow</a>
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
      <div class="item">
        <div class="pos-rlt">
          <div class="item-overlay opacity r r-2x bg-black">
            <div class="center text-center m-t-n">
              <a href="#"><i class="fa fa-play-circle i-2x"></i></a>
            </div>
          </div>
          <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
        </div>
        <div class="padder-v">
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis">Tempered Song</a>
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis text-xs text-muted">Miaow</a>
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
      <div class="item">
        <div class="pos-rlt">
          <div class="item-overlay opacity r r-2x bg-black">
            <div class="center text-center m-t-n">
              <a href="#"><i class="fa fa-play-circle i-2x"></i></a>
            </div>
          </div>
          <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
        </div>
        <div class="padder-v">
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis">Tempered Song</a>
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis text-xs text-muted">Miaow</a>
        </div>
      </div>
    </div>
    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
      <div class="item">
        <div class="pos-rlt">
          <div class="item-overlay opacity r r-2x bg-black">
            <div class="center text-center m-t-n">
              <a href="#"><i class="fa fa-play-circle i-2x"></i></a>
            </div>
          </div>
          <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
        </div>
        <div class="padder-v">
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis">Tempered Song</a>
          <a href="track-detail.html" data-bjax="" data-target="#bjax-target" data-el="#bjax-el" data-replace="true" class="text-ellipsis text-xs text-muted">Miaow</a>
        </div>
      </div>
    </div>
</section>

@endsection
