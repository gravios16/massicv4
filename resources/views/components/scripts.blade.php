<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ URL::asset('js/bootstrap.js') }}"></script>
<!-- App -->
<script src="{{ URL::asset('js/app.js') }}"></script>  
<script src="{{ URL::asset('js/slimscroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ URL::asset('js/app.plugin.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jPlayer/jquery.jplayer.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jPlayer/add-on/jplayer.playlist.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jPlayer/demo.js') }}"></script>
<script src="{{ URL::asset('js/socket.io/socket.io.js') }}"></script>