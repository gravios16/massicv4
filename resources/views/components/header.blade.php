<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('js/jPlayer/jplayer.flat.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}" type="text/css" />  
    <!--[if lt IE 9]>
        <script src="js/ie/html5shiv.js"></script>
        <script src="js/ie/respond.min.js"></script>
        <script src="js/ie/excanvas.js"></script>
    <![endif]-->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">    

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>