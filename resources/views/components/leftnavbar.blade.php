<!-- .aside -->
<aside class="bg-black dk aside hidden-print" id="nav">
  <section class="vbox">
    <section class="w-f-md scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
        <!-- nav -->
        <nav class="nav-primary hidden-xs">
          <ul class="nav bg clearfix">
            <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
              Inicio 
            </li>
            <li>
              <a href="{{route('home')}}">
                <i class="icon-home icon text-primary"></i>
                <span class="font-bold">Estudios</span>
              </a>
            </li>
            <li>
              <a href="genres.html">
                <i class="icon-music-tone-alt icon text-info"></i>
                <span class="font-bold">Géneros</span>
              </a>
            </li>
            <li>
              <a href="events.html">
                <i class="icon-drawer icon text-primary-lter"></i>
                <b class="badge bg-primary pull-right">6</b>
                <span class="font-bold">Eventos</span>
              </a>
            </li>
            <li>
              <a href="listen.html">
                <i class="icon-list icon  text-info-dker"></i>
                <span class="font-bold">Escucha</span>
              </a>
            </li>
            <li>
              <a href="video.html" data-target="#content" data-el="#bjax-el" data-replace="true">
                <i class="icon-social-youtube icon  text-primary"></i>
                <span class="font-bold">Videos</span>
              </a>
            </li>
            <li class="m-b hidden-nav-xs"></li>
          </ul>
          @if ( !Auth::guest() && Auth::user()->userStudio !== null)
          <ul class="nav text-sm">
            <li class="hidden-nav-xs padder m-t m-b-sm text-xs text-muted">
              Mi estudio{{  Auth::user()->userStudio->studio_id }}
            </li>
            <li>
              <a href="#">
                <i class="fa fa-map-marker text-success-lter"></i>
                <span>Sedes</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="fa-microphone fa text-success-lter"></i>
                <b class="badge bg-success dker pull-right hide">9</b>
                <span>Salas</span>
              </a>
            </li>
            <li>
            <li>
              <a href="#">
                <i class="icon-equalizer icon text-success-lter"></i>
                <b class="badge bg-success dker pull-right hide">9</b>
                <span>Equipos</span>
              </a>
            </li>
            <li>
              <a href="#">
                <i class="icon-calendar icon text-success-lter"></i>
                <b class="badge bg-success dker pull-right hide">9</b>
                <span>Reservas</span>
              </a>
            </li>
          </ul>
          @endif
        </nav>
        <!-- / nav -->
      </div>
    </section>
    
    <footer class="footer hidden-xs no-padder text-center-nav-xs">
      <div class="bg hidden-xs ">
          <div class="dropdown dropup wrapper-sm clearfix">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="thumb-sm avatar pull-left m-l-xs">                        
                <img src="{{ URL::asset('musik_images/a3.png') }}" class="dker" alt="...">
                <i class="on b-black"></i>
              </span>
              <span class="hidden-nav-xs clear">
                <span class="block m-l">
                  <strong class="font-bold text-lt">John.Smith</strong> 
                  <b class="caret"></b>
                </span>
                <span class="text-muted text-xs block m-l">Art Director</span>
              </span>
            </a>
            <ul class="dropdown-menu animated fadeInRight aside text-left">                      
              <li>
                <span class="arrow bottom hidden-nav-xs"></span>
                <a href="#">Settings</a>
              </li>
              <li>
                <a href="profile.html">Profile</a>
              </li>
              <li>
                <a href="#">
                  <span class="badge bg-danger pull-right">3</span>
                  Notifications
                </a>
              </li>
              <li>
                <a href="docs.html">Help</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="modal.lockme.html" data-toggle="ajaxModal" >Logout</a>
              </li>
            </ul>
          </div>
        </div>            </footer>
  </section>
</aside>
<!-- /.aside -->