@extends('layouts.app', ['c_player'=>true])

@section('content')
<section class="scrollable padder-lg w-f-md" id="bjax-target">

    <a href="#" class="pull-right text-muted m-t-lg" data-toggle="class:fa-spin" >
            <i class="icon-refresh i-lg    inline" id="refresh"></i>
    </a>
    <h2 class="font-thin m-b">
        Estudios
        <span class="musicbar animate inline m-l-sm" style="width:20px;height:20px">
            <span class="bar1 a1 bg-primary lter"></span>
            <span class="bar2 a2 bg-info lt"></span>
            <span class="bar3 a3 bg-success"></span>
            <span class="bar4 a4 bg-warning dk"></span>
            <span class="bar5 a5 bg-danger dker"></span>
        </span>
    </h2>

    <div class="row row-sm">
        @foreach ($studios as $studio)
            @foreach ($studio["headquarters"] as $headquarter)
                @if( sizeof($headquarter["studio_rooms"]) > 0 )
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <a href="{{route('studioProfile', ['id' => $studio['id']])}}" class="">
                                        {{$studio["name"]}}
                                        @if( sizeof($studio["headquarters"]) > 1 )
                                            - {{$headquarter["name"]}}
                                        @endif
                                    </a>
                                </h3>
                            </div>
                                <!-- .crousel fade -->
                                <section class="bg-black">
                                    <div class="carousel slide panel-body" id="c-slide-{{$headquarter['id']}}" style="padding: 30px;">
                                            <ol class="carousel-indicators out">
                                                @foreach ($headquarter["studio_rooms"] as $index => $studio_room)
                                                    @if( sizeof($studio_room["resources"]) > 0 )
                                                    <li data-target="#c-slide-{{$headquarter['id']}}" data-slide-to="0" class="@if($index==0)active @endif"></li>
                                                    @endif
                                                @endforeach
                                            </ol>
                                            <div class="carousel-inner">
                                                @foreach ($headquarter["studio_rooms"] as $index => $studio_room)
                                                    @if( sizeof($studio_room["resources"]) > 0 )
                                                    <div class="item @if($index==0)active @endif">
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                                <div class="pos-rlt">
                                                                    <div class="item-overlay opacity r r-2x bg-black">
                                                                        <div class="center text-center m-t-n">
                                                                            <a href="#"><i class="fa fa-play-circle i-2x"></i></a>
                                                                        </div>
                                                                    </div>
                                                                    <a href="track-detail.html"><img src="http://musik.app/images/m1.jpg" alt="" class="r r-2x img-full"></a>
                                                                </div>
                                                        </div>
                                                        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                                            <p class="text-center">
                                                                <em class="h3 text-mute"><a href="#">{{$studio_room['name']}}</a></em><br>
                                                                <small class="text-muted">
                                                                    <ul>
                                                                    @foreach ($studio_room["resources"] as $resource)
                                                                        <li>
                                                                            {{$resource['name']}} ({{$resource['brand']}})
                                                                        </li>
                                                                    @endforeach
                                                                    </ul>
                                                                </small>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    @endif
                                                @endforeach
                                            </div>

                                            <a class="left carousel-control" href="#c-slide-{{$headquarter['id']}}" data-slide="prev">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                            <a class="right carousel-control" href="#c-slide-{{$headquarter['id']}}" data-slide="next">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                    </div>
                                </section>
                                <!-- / .carousel fade -->
                      
                        </div>
                        
                    </div>
                @endif
            @endforeach
        @endforeach
    </div>


</section>

@endsection
